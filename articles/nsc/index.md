# nsc - Neocities Shell Client

Neocities has some cli clients floating around for interacting with it's
[API](https://neocities.org/api). But most of them are a bit... much (at least
for me). So I've been thinkin' about writing my own client for Neocities.
Maybe as a shell script that interacts with the API with some other tool.

So I made [nsc](https://codeberg.org/kktk/nsc). `nsc` is a shell script that
uses curl for interacting with Neocities' API. Currently it can only
delete/upload files and get some raw JSON info from Neocities sites.

I'm thinking of adding a push command that recursively uploads updated files.
Maybe an option for specifying a directory for where files should go when
uploading too.

PS. Yes I know this will probably be completely useless once I move to a
different platform

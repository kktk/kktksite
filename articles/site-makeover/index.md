# Site "Make"-over

I've decided to move to static site generation (using POSIX make) for building
my website. Reasons being that I'm starting to realize why some people [hate
XML stuff](http://harmful.cat-v.org/software/xml).

The benefit of this is that I get to use a document structure I actually like.
There's also a "XHTML Only" option now in the footer, in case some people like
or even need that. I've chosen markdown for the language before HTML, though
I might turn to [CommonMark](https://commonmark.org) in the future (for
compatibility's sake).

For more details on how it's built, see the [website's
code](https://codeberg.org/kktk/kktksite). Yes, I have git repositories now;
hosted by the, admittedly, very generous people over at
[Codeberg](https://codeberg.org).

I originally planned on using [SourceHut](https://sr.ht) or even renting a VPS
first for hosting my *own* git. Though I don't currently have enough
resources for those in the long term. And because I felt that waiting for
those would take a bit too long, I decided to go with Codeberg for the time
being. Mark my words, I will self-host one day!  One day...

# I Love PNGs.

I love PNGs, they're pretty much necessary for my workflow since I mostly
do art with no anti-aliasing which makes it the perfect format. It's
almost uncanny in how well it suits my needs. It's also a pretty old and
well defined standard which means there are a lot of image viewers and
tools that support it. It would probably be more surprising if something
*didn't* support PNGs, can you imagine that?

I have not yet seen an alternative that matches it's great compression and
handling of pixels. I've tried other formats like [FLIF](https://flif.info)
and [JPEGXL](https://jpegxl.info) before and thought they were pretty cool,
though I've tried the latter more than the former. For some context FLIF was
supposed to be a "Free Lossless Image Format" (hence the acronym FLIF) and
ever since it was abandoned, there have been image formats such as
Cloudinary's [FUIF](https://github.com/cloudinary/fuif) and Google's
[Pik](https://github.com/google/pik) proposal. Then came JPEGXL, taking ideas
and inspiration from FLIF and it's descendants.

I tried JPEGXL and I was pretty amazed at how it was able to compress
paintings into almost single digit kilobyte files, but notice how I said
"paintings". You might know where I'm going with this but PNG is still king
when it comes to pixel art or just art with no anti-aliasing. The reason I
say this is because I actually tried compressing one of my usual digital
sketches and turns out that the compressed PNG version was smaller **and**
looked better than the JPEGXL one. While JPEGXL could easily compress it into
a single digit kilobyte file, if I wanted the image to be pixel-perfect (no
artifacts) I would have to use the mathematically lossless compression of
JPEGXL which ended up as a larger file in comparison to the PNG one compressed
with [pngquant](https://pngquant.org).

Speaking of `pngquant`, this tool is absolutely amazing. I'm still baffled to
this day how it can almost consistently *half* the sizes of PNGs, and the way
it compresses things leads to interesting results. Basically what it does is
(if there's a lot of colors) limit the color palette to a certain degree and
uses dithering to blend things, unless specified otherwise. This method
results in *significantly* smaller file sizes and gives images a somewhat
nostalgic feel due to the use of a limited color palette and dithering. It
almost feels like how images used to be compressed to fit within the color
limitations of old hardware.

[![pngquant_examp](pix/pngquant_examp.png)](pix/pngquant_examp.png)

Best part about using pngquant is that since it's based on limiting the color
palette, I can almost consistently compress my sketches two-fold with
literally 0 loss in quality since almost all of my sketches are basically
monochrome. I love it!

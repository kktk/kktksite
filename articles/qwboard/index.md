# Review: *Quartet* Magnetic, Dry-Erase Whiteboard

*Overall: Recommend*

[![qwboard_front](pix/front.png)](pix/front.png)

Well well well, here I am with a review of a whiteboard.

The reason I bought this is because I watched a lecture by the great
[Marshall Vandruff](https://marshallart.com) about
[how amazing a whiteboard is](https://yewtu.be/watch?v=AblcTu7WFlA) and just
thought I **had** to have one. Plus I was already doing things like
clustering (as you see in the image above) and weekly-scheduling (which I
might talk about separately) that would greatly benefit from a whiteboard,
because previously I would use pencil &amp; paper which is comparitively not
as smooth or rapid feeling as something like a whiteboard. As for the reason
of reviewing it, that's just my duty as a consumer. I am not affiliated with
Quartet and I highly doubt they even know who I am, I was just searching for a
nicely priced and built whiteboard that was moderately sized. This review is
also originally written after a week of using it, because immediately
reviewing things after purchase has (in my experience) lead to some misleading
or quickly dated reviews.

The build quality is solid; It's got a nice weight to it and it doesn't feel
cheap. Only exception would be the plastic frame which is just barely wobbly
and feels weaker than the whiteboard itself. Though it's not much of a
problem, I would recommend buying one with an aluminium frame for an extra
feeling of heft. The frame also has four squares on the edges of the back for
mounting it with the mounting pads it comes with, though I'm probably not
gonna use those since I planned on just carrying the thing whenever I need it.

[![qwboard_back](pix/back.png)](pix/back.png)

The board I bought comes with a few extras; a black marker with an eraser cap,
2 magnets, and 4 of the previously mentioned mounting pads.

[![qwboard_bundle](pix/bundle.png)](pix/bundle.png)

There's also this weird little plastic holder for the marker which I'm unsure
if is just specific to the one I bought?  I'm not too sure how to feel about
it, it's just clear plastic to me. Makes for a nice picture though.

[![qwboard_holder](pix/holder.png)](pix/holder.png)

The marker draws pretty well and the eraser cap is handy at times, though I
would recommend buying the marker and eraser separately if you want something
of higher quality. The eraser cap also has a magnet on the back for sticking
it and/or the marker to the board.

[![qwboard_marker-draw](pix/marker-draw.png)](pix/marker-draw.png)
[![qwboard_marker](pix/marker.png)](pix/marker.png)

When erasing, there's a *very* faint smudge/ghost of what was previously drawn
but I'm unsure whether that's a problem with the eraser or the whiteboard
itself. Again the smudge/ghost is *very* faint so it's not too big of a deal.

[![qwboard_smudge](pix/smudge.png)](pix/smudge.png)

As for the magnets and the general magnetic aspect of the board, they stick
very well; It's going to take a lot of vigorous shaking to make them
accidentally fall off. Though I'm personally not gonna use them very much,
they would be handy for sticking some notes or other pieces of paper to the
board.

[![qwboard_magnets](pix/magnets.png)](pix/magnets.png)

I had never heard of Quartet before buying this board so I had no idea what
kind of quality I was gonna get. They seem to be a pretty standard company,
JavaScript website and the sorts, though I was a bit surprised with all the
kinds of boards they offered. They had boards for scheduling and others were
even a mix of planner and tackboard. As a final note I will mention that the
sizes of the boards are a bit peculiar, they're just barely shorter than they
usually are (45x60cm becomes 43x58cm, same for inches).

All in all I am pleasantly pleased for what amounts to a medium sized,
personal whiteboard for under $20, would definitely recommend Quartet if a
friend/family member needs some sort of home board to work on and want a
starting point for where to look. Though keep in mind that this is the only
board I've actively sought out for and bought with my own money so I don't
really have any more context for how a whiteboard "should be" or whatever,
this is a just a review from an artist for the layman. If you want some sort
of "*buyer's guide*" then you might want to take a look at the bottom part of
[this list](https://www.architecturelab.net/best-dry-erase-boards-and-whiteboards),
website has some javascript and a lot of trackers though so stay safe.

## Overall: Recommend

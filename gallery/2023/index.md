# 2023 Art

[![stylestudy-jetblack](stylestudy-jetblack.png)](stylestudy-jetblack.png)

## Older

[![stylestudy-edeinfaye](stylestudy-edeinfaye.png)](stylestudy-edeinfaye.png)
[![stylestudy-spikespiegel](stylestudy-spikespiegel.png)](stylestudy-spikespiegel.png)
[![stylestudy-douyincos](stylestudy-douyincos.png)](stylestudy-douyincos.png)
[![stylestudy-minabass](stylestudy-minabass.png)](stylestudy-minabass.png)
[![stylestudy-gojo](stylestudy-gojo.png)](stylestudy-gojo.png)
[![tumbling-light](tumbling-light.png)](tumbling-light.png)
[![tumbling](tumbling.png)](tumbling.png)
[![gradmap-fun](gradmap-fun.png)](gradmap-fun.png)
[![competition-breeds-creation](competition-breeds-creation.png)](competition-breeds-creation.png)

## Even Older

- [ryo-yamada-flcl](ryo-yamada-flcl.png)
- [will-baxter](will-baxter.png)
- [culture-night](culture-night.png)
- [cogan](cogan.png)
- [outnightsea](outnightsea.png)
- [geisha-scrap](geisha-scrap.png)
- [denji-paint](denji-paint.png)
- [midnight-melody](midnight-melody.png)
- [desusakana](desusakana.png)
- [ballebot](ballebot.png)
- [mindtaken](mindtaken.png)

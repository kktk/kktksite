# About

Hello! I'm kaka ("shit" in estonian :v), a digital illustrator who loves to
draw based on deep understanding of the fundamentals. I love to learn and lay
things out in clear, simple, and practical terms. I love to share obscure
tools, content, and resources that you'd also LOVE but might not find on your
own. I've learned things the hard way so *you* don't have to.

This site is statically generated using a combination of md files,
[mk](https://9fans.github.io/plan9port/man/man1/mk.html), and my own tool
[stev](https://codeberg.org/kktk/stev) (you can check out the code
[here](https://codeberg.org/kktk/kktksite) if you want).

## Contact

*Tell me your deepest desires :V*

My email: [kktkmailing *at* disroot *dot* org](mailto://kktkmailing@disroot.org)

My [PGP](files/kktkpub.asc) key:

	pub   rsa4096 2023-04-07 [SC] [expires: 2025-04-06]
	      2D35 1AFD E834 9801 278C  F774 7660 8971 4F84 6FC7
	uid           [ultimate] KakaTekel <kktkmailing@disroot.org>
	sub   rsa4096 2023-04-07 [E] [expires: 2025-04-06]

Mastodon: @kakatekel@mastodon.social

## Donate

You can donate by sending a tip through [my Neocities
page](https://neocities.org/site/kktk) or my
[Ko-Fi](https://ko-fi.com/KakaTekel).

## License

The *contents* of this site are licensed under the
[OWL](https://owl.apotheon.org/):

```
# Open Works License

This is version 0.9.4 of the Open Works License

## Terms

Permission is hereby granted by the holder(s) of copyright or other legal
privileges, author(s) or assembler(s), and contributor(s) of this work, to any
person who obtains a copy of this work in any form, to reproduce, modify,
distribute, publish, sell, sublicense, use, and/or otherwise deal in the
licensed material without restriction, provided the following conditions are
met:

Redistributions, modified or unmodified, in whole or in part, must retain
applicable copyright and other legal privilege notices, the above license
notice, these conditions, and the following disclaimer.

NO WARRANTY OF ANY KIND IS IMPLIED BY, OR SHOULD BE INFERRED FROM, THIS LICENSE
OR THE ACT OF DISTRIBUTION UNDER THE TERMS OF THIS LICENSE, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE,
AND NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS, ASSEMBLERS, OR HOLDERS OF
COPYRIGHT OR OTHER LEGAL PRIVILEGE BE LIABLE FOR ANY CLAIM, DAMAGES, OR OTHER
LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT, OR OTHERWISE ARISING FROM, OUT
OF, OR IN CONNECTION WITH THE WORK OR THE USE OF OR OTHER DEALINGS IN THE WORK.
```

The *[code]($CODE/kktksite)* for this site is licensed under the
[Unlicense](https://unlicense.org/):

```
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <https://unlicense.org/>
```

### Copyrighted Works

The following works (possibly) are and/or contain similar copies or
derivatives of other copyrighted works and I do not have permission to license
them:

- [14th - Echo Gecko](gallery/2020/14th%20-%20Echo%20Gecko.png)
- [15th - MirbyGames](gallery/2020/15th%20-%20MirbyGames.png)
- [17th - Glitch](gallery/2020/17th%20-%20Glitch.png)
- [19th - Quartermoon](gallery/2020/19th%20-%20Quartermoon.png)
- [23rd - QueenSterPop](gallery/2020/23rd%20-%20QueenSterPop.png)
- [24th - Custard](gallery/2020/24th%20-%20Custard.png)
- [31st - Krypt1k R3d](gallery/2020/31st%20-%20Krypt1k%20R3d.png)
- [5mStudies1](gallery/2021/5mStudies1.png)
- [5mStudies2](gallery/2021/5mStudies2.png)
- [5mStudies3](gallery/2021/5mStudies3.png)
- [5mStudies4](gallery/2021/5mStudies4.png)
- [6mStudies](gallery/2021/6mStudies.png)
- [AYPETTER](gallery/2020/AYPETTER.png)
- [BackgroundCorruption](gallery/2020/BackgroundCorruption.png)
- [Balls](gallery/2021/Balls.png)
- [Beep](gallery/2021/Beep.png)
- [Beherit](gallery/2021/Beherit.png)
- [Ben10](gallery/2021/Ben10.png)
- [Bleck](gallery/2021/Bleck.png)
- [BoyHasPhish](gallery/2020/BoyHasPhish.png)
- [CTPKrima](gallery/2020/CTPKrima.png)
- [CTPSquares](gallery/2021/CTPSquares.png)
- [Charow](gallery/2021/Charow.png)
- [Choke](gallery/2021/Choke.png)
- [ColoBall](gallery/2021/ColoBall.png)
- [Crystal](gallery/2021/Crystal.png)
- [Dod](gallery/2021/Dod.png)
- [Dool](gallery/2021/Dool.png)
- [DrapperyGETREAL](gallery/2021/DrapperyGETREAL.png)
- [EEHEHEHEHE](gallery/2021/EEHEHEHEHE.png)
- [EggOfTrollge](gallery/2021/EggOfTrollge.png)
- [FinalNormal](gallery/2020/FinalNormal.png)
- [Fishbubbly Jubbly](gallery/2021/Fishbubbly%20Jubbly.png)
- [GIve](gallery/2020/GIve.png)
- [Gamer](gallery/2021/Gamer.png)
- [Gerg](gallery/2020/Gerg.png)
- [Goldix](gallery/2021/Goldix.png)
- [Gorn](gallery/2021/Gorn.png)
- [Hair](gallery/2021/Hair.png)
- [HandStudies](gallery/2021/HandStudies.png)
- [Herb](gallery/2020/Herb.png)
- [Hnng](gallery/2021/Hnng.png)
- [ILikeToRideMyBicyle](gallery/2021/ILikeToRideMyBicyle.png)
- [ILoveGuns](gallery/2021/ILoveGuns.png)
- [Jifs](gallery/2020/Jifs.png)
- [Joshistairs](gallery/2021/Joshistairs.png)
- [Kabumie](gallery/2021/Kabumie.png)
- [Kaiju Battle](gallery/2020/Kaiju%20Battle.png)
- [LimbsAndGenieVinceKoumanbyZaulman](gallery/2021/LimbsAndGenieVinceKoumanbyZaulman.png)
- [LostDonuts](gallery/2020/LostDonuts.png)
- [Matthew](gallery/2021/Matthew.png)
- [MeetKaka](gallery/2021/MeetKaka.png)
- [Metal Gear Triforce](gallery/2020/Metal%20Gear%20Triforce.png)
- [MyRigSunglasses](gallery/2021/MyRigSunglasses.png)
- [Nardo](gallery/2020/Nardo.png)
- [OS28](gallery/2019/OS28.webp)
- [OS29](gallery/2019/OS29.webp)
- [OS30](gallery/2019/OS30.webp)
- [OS31](gallery/2019/OS31.webp)
- [OS32](gallery/2019/OS32.webp)
- [OS33](gallery/2019/OS33.webp)
- [OS35](gallery/2019/OS35.webp)
- [OS36](gallery/2019/OS36.webp)
- [OS37](gallery/2019/OS37.webp)
- [OS39](gallery/2019/OS39.webp)
- [OS41](gallery/2019/OS41.webp)
- [OS44](gallery/2019/OS44.webp)
- [OS45](gallery/2019/OS45.webp)
- [OS47](gallery/2019/OS47.webp)
- [OS48](gallery/2019/OS48.webp)
- [OS51](gallery/2019/OS51.webp)
- [OS53](gallery/2019/OS53.webp)
- [OS54](gallery/2019/OS54.webp)
- [OS55](gallery/2019/OS55.webp)
- [OS56](gallery/2019/OS56.webp)
- [OnEdge](gallery/2021/OnEdge.png)
- [POGGERS](gallery/2020/POGGERS.png)
- [PVZ](gallery/2021/PVZ.png)
- [Peencil](gallery/2021/Peencil.png)
- [Rick](gallery/2021/Rick.png)
- [RodentGaming](gallery/2020/RodentGaming.png)
- [RoverFromRealinFake](gallery/2020/RoverFromRealinFake.png)
- [SANS](gallery/2020/SANS.png)
- [SHRED](gallery/2021/SHRED.png)
- [SadBoi](gallery/2020/SadBoi.png)
- [Sadbois](gallery/2021/Sadbois.png)
- [Scorch](gallery/2020/Scorch.png)
- [Search](gallery/2021/Search.png)
- [ShallNot](gallery/2021/ShallNot.png)
- [StudyStudyStudy](gallery/2021/StudyStudyStudy.png)
- [SuperSmegma](gallery/2021/SuperSmegma.png)
- [TankyBanky](gallery/2020/TankyBanky.png)
- [Toothbrush](gallery/2020/Toothbrush.png)
- [Vermin](gallery/2021/Vermin.png)
- [Vile](gallery/2021/Vile.png)
- [Void](gallery/2021/Void.png)
- [Warden](gallery/2020/Warden.png)
- [Wii Bros Fanart](gallery/2020/Wii%20Bros%20Fanart.png)
- [YEA](gallery/2021/YEA.png)
- [YooIsThat](gallery/2021/YooIsThat.png)
- [YoshinariStudies](gallery/2021/YoshinariStudies.png)
- [Zenith](gallery/2020/Zenith.png)
- [backing2](gallery/2021/backing2.png)
- [chowda](gallery/2021/chowda.png)
- [cluster_golden-time](gallery/2021/cluster_golden-time.png)
- [colfun](gallery/2021/colfun.png)
- [denji-paint](gallery/2023/denji-paint.png)
- [dp001](gallery/2022/dp001.png)
- [eva01](gallery/2021/eva01.png)
- [ganyu-mogoonyoshinari](gallery/2024/ganyu-mogoonyoshinari.png)
- [hermione-deathbook](gallery/2024/hermione-deathbook.png)
- [masswing](gallery/2021/masswing.png)
- [mogoon-formstudy](gallery/2024/mogoon-formstudy.png)
- [operating_system](gallery/2021/operating_system.png)
- [pavolia-glasses](gallery/2024/pavolia-glasses.png)
- [pisschamp](gallery/2021/pisschamp.png)
- [pup](gallery/2021/pup.png)
- [rranger-fighterd](gallery/2024/rranger-fighterd.png)
- [ryo-yamada-flcl](gallery/2023/ryo-yamada-flcl.png)
- [study-cloth-marchingband](gallery/2024/study-cloth-marchingband.png)
- [study-cloth-officeshirt](gallery/2024/study-cloth-officeshirt.png)
- [study-clothnotes](gallery/2024/study-clothnotes.png)
- [study-hair-gabimaru](gallery/2024/study-hair-gabimaru.png)
- [study-head-lowangle](gallery/2024/study-head-lowangle.png)
- [study-lfva-01ballcover](gallery/2024/study-lfva-01ballcover.png)
- [study-lfva-02camdawn](gallery/2024/study-lfva-02camdawn.png)
- [study-mogoonhair](gallery/2024/study-mogoonhair.png)
- [study-yohmugieyes](gallery/2024/study-yohmugieyes.png)
- [study_breakwind](gallery/2022/study_breakwind.png)
- [stylestudy-douyincos](gallery/2023/stylestudy-douyincos.png)
- [stylestudy-edeinfaye](gallery/2023/stylestudy-edeinfaye.png)
- [stylestudy-gojo](gallery/2023/stylestudy-gojo.png)
- [stylestudy-jetblack](gallery/2023/stylestudy-jetblack.png)
- [stylestudy-minabass](gallery/2023/stylestudy-minabass.png)
- [stylestudy-spikespiegel](gallery/2023/stylestudy-spikespiegel.png)
- [takuto-light-new](gallery/2024/takuto-light-new.png)
- [takuto-light](gallery/2024/takuto-light.png)
- [ventisuit](gallery/2024/ventisuit.png)
- [violet-evergarden](gallery/2024/violet-evergarden.png)
- [waluiKILL](gallery/2021/waluiKILL.png)
- [wanderer-tongue](gallery/2024/wanderer-tongue.png)
- [we_do_a_little](gallery/2021/we_do_a_little.png)

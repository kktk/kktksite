# KakaTekel's Website

[![web_atdesk](gallery/2021/web_atdesk.png)](gallery/2021/web_atdesk.png)

Y'ello, welcome to my site!

I'm kaka ("shit" in estonian :v), I'm a digital illustrator who loves to draw
based on deep understanding of the fundamentals. I love to learn and lay
things out in clear, simple, and practical terms. I love to share obscure
tools, content, and resources that you'd also LOVE but might not find on your
own. I've learned things the hard way so *you* don't have to.

Enjoy!

## Latest Work

[![rranger-fighterd](gallery/2024/rranger-fighterd.png)](gallery/2024/rranger-fighterd.png)

## Thing of the Day

The types of people you dislike the most are usually a reflection of your worst traits.

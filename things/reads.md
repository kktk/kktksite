# Reads

Some reading material I'd like to share.

## Articles

- [Against an Increasingly User-Hostile Web](https://neustadt.fr/essays/against-a-user-hostile-web): Article on how the Web has become increasingly trash overtime
- [Ask questions the smart way](http://www.catb.org/~esr/faqs/smart-questions.html): Title says it all
- [Matrix vs. XMPP](https://lukesmith.xyz/articles/matrix-vs-xmpp): A comparison between Matrix and XMPP
- [Neon Genesis Evangelion: An article about color correcting the hideous Renewal's Episode 19 BD.](https://sephirotic.wordpress.com/2015/10/11/neon-genesis-evangelion-an-article-about-color-correction-the-hideous-renewals-episode-19): I have no idea how I found this, still interesting though
- [Pessimism is for Losers](https://jacobwsmith.xyz/stories/pessimism.html): Stop moping and start doing something
- [Reject Discord, Embrace Matrix!](https://monstro1.com/fuckdiscord.html): Article about why you should use Matrix over something like Discord
- [The Six-Lesson Schoolteacher](https://www.cantrip.org/gatto.html): Essay on the failure that is The American School System and what we can do about it
- [honeypot.im ToS](https://honeypot.im/terms-of-service): Cute terms of service :-)

## Books

A list of books that I would highly recommend or at the **very** least
are interesting/good reads.

- Rudolf Flesch - The Art of Clear Thinking
- Rudolf Flesch - The Art of Plain Talk

### Academical

- Robert Carman - Quick Arithmetics

### Art

- Andrew Loomis - Creative Illustration
- Cutler & Cutler - J. C. Leyendecker
- Gabriel Rico - Writing The Natural Way: **Fantastic** writing book on how to naturally be more creative, applicable to any creative process.
- Molly Bang - Picture This: Nice and short book that teaches composition principles using paper cut-outs et al.
- Richard Williams - The Animator's Survival Kit (Expanded Edition)
- Robert Beverly Hale - Drawing Lessons from the Great Masters
- Scott Robertson - How to Draw
- Steven Pressfield - The War of Art: Describes "resistance", the enemy of creativity, how it disfigures our lives, and gives us a battle plan to face it with.
- The Art of Yoh Yoshinari: Rough Sketches: Absolutely breathtaking sketches from You Yoshinari, a Gainax veteran and Trigger director. [Mark]($YT/channel/UCYXLyantCxfr6uF6zUTxQLg) made a [3 hour stream]($YT/watch?v=FBq3mVQVveQ) analyzing the book.

### Life

- Dale Carnegie - How to Win Friends and Influence People
- Joel Salatin - Everything I Want to Do Is Illegal
- Stephen R. Covey - The 7 Habits of Highly Effective People: A **magnificent** book that I wish everyone would read and apply to their lives.
- Tom Venuto - Burn the Fat, Feed the Muscles

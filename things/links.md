# Links

## More Links

- [Farside](https://farside.link): Links to alternative frontends for bigger services like YouTube, Twitter/X, etc.
- [Let's Decentralize](https://letsdecentralize.org): Links to sites on the hidden web and other protocols
- [Suckless - rocks](https://suckless.org/rocks): Collection of software that "rocks". Most are good/great, some are a bit outdated
- [XXIIVV](https://webring.xxiivv.com): "...an attempt to inspire artists & developers to build their websites and share traffic amongst each other"

## Websites

- [9front](https://9front.org): Actively developed fork of plan9
- [Bikobatanari](https://bikobatanari.art): Great guy who draws, writes, and inspired me to make my own site
- [Cadence](https://cadence.moe): Hosts [CloudTube](https://tube.cadence.moe) and inspired one of my videos
- [Drew Devault](https://drewdevault.com) ([gemini](gemini://drewdevault.com)): Made Sway, [Hare](https://harelang.org), and many other great tools
- [Jaquin](https://jaquin.net): Animator with a knack for color, similar to Bassetfilms. Also seems to like cats.
- [Occasionally, content](https://murid.neocities.org): Good artist that mostly draws in pen and ink, has writings too.
- [Sentence Search](https://sentencesearch.neocities.org): Really nice for searching voiced examples to grok a certain japanese word/phrase
- [Yujiri - Reasons to become tech-literate](https://yujiri.xyz/software/why-program.gmi):
- [jpdb](https://jpdb.io): Very featureful japanese-english dictionary, specifically like how detailed it's sentence breakdowns are and the impressive amount of voiced example sentences it has

## YouTube Channels

- [BestGuyEver]($YT/channel/UCGgy9QqFwElrVg4vf6QNX_A)
- [CS Ghost Animation]($YT/channel/UC1rf511TyJlFICTvOZhHo1w)
- [Gamefromscratch]($YT/channel/UCr-5TdGkKszdbboXXsFZJTQ): Mostly watch for the game/software news
- [Hobbes Sakuga]($YT/channel/UCaA5wWERI1gyl-iwTkb0JTw): Loves animation, great music, and clearly has pride in his work and strives to improve it
- [Just a Beginner Drawer]($YT/channel/UCq9KHjgRVgJZK-1K9HlAldA): A friend :')
- [KakaTekel]($YT/channel/UCnMKMbzZwtH_GukDD-pCRGw): My channel :^)
- [Linus Boman]($YT/channel/UCSb-xYOELhIqVr2n9s-b_4w): Underrated channel with high quality, mostly essay/story style videos on graphic design (typography et al.)
- [NIRO]($YT/channel/UCpU6wzwKXR2BQnd9nBdlIXw): Similar to tppo but more so stream of thought. NIRO and tppo taught me an important lesson; If you want to draw like the masters, then just... draw like them. Steal their ideas, copy specific aspects of their work, and use references so you actually know what things look like
- [Proko]($YT/channel/UClM2LuQ1q5WEc23462tQzBg): *The* art youtube channel
- [Skrib]($YT/channel/UC6eujEHX2UNWP3Vjx2HgKLQ): Underrated artist with a style I personally enjoy a lot
- [Tawaci]($YT/channel/UCsbL2we-zLnNaxVM3AYX9gw): "Just the average guy" -Tawaci
- [guzzu]($YT/channel/UCr7cJxCH5OVfYXn31256I-g): A personal animation inspiration of mine
- [tppo]($YT/channel/UC2PqMuVxs-y6zBJjNEyZa5A): Really well edited videos studying professional (and actually really fuckin' good) anime illustrators from a technical aspect but without forgetting the fundamentals that are at the core of any good artwork

## Android

- [Another notes app](https://github.com/maltaisn/another-notes-app): Has the nicest *feel* compared to other notes apps
- [Audio Recorder](https://f-droid.org/en/packages/com.github.axet.audiorecorder): Simple but has some nice settings
- [Bitmask](https://bitmask.net): Provides VPNs from Calyx and Riseup
- [Diary](https://f-droid.org/en/packages/org.billthefarmer.diary): Mostly for taking temporary notes
- [F-Droid Classic](https://f-droid.org/en/packages/eu.bubu1.fdroidclassic): A bit more functional (personally) compared to the more modern looking client
- [GrapheneOS](https://grapheneos.org): Android ROM I personally use, specifically like the extra settings (mic & video disabling) and how well the apps integrate with the OS. Especially love the camera app
- [KISS Launcher](https://github.com/Neamar/KISS): Hits that sweet spot of being minimal yet customizable
- [Mozc for Android](https://f-droid.org/en/packages/org.mozc.android.inputmethod.japanese): Pretty good & featureful japanese keyboard
- [Nani?](https://nani.lepiller.eu): Offline japanese dictionary. I like it's speed and the fact that you can highlight text while reading something and search it in the app
- [Newpipe](https://github.com/TeamNewPipe/NewPipe): Best YouTube client. No ads, local playlists, awesome download feature, and a feed that's basically just a list of recent videos from your subscriptions
- [Shelter](https://f-droid.org/en/packages/net.typeblog.shelter): App for sandboxing apps in a work profile. Specifically like the auto-freeze feature
- [SicMu Player](https://f-droid.org/en/packages/souch.smp): Very nice & functional folder-based music player
- [Simple Calendar](https://github.com/SimpleMobileTools/Simple-Calendar): I like that it can add some general events and holidays based on the region you give it
- [Tasks.org](https://github.com/tasks/tasks): Specifically like that you can add subtasks and how it sorts tasks with deadlines in proper chronological order
- [Unexpected Keyboard](https://github.com/Julow/Unexpected-Keyboard): Really nice and functional keyboard
- [VLC](https://videolan.org/vlc/index.html): When mpv-android can't play certain files
- [mpv-android](https://github.com/mpv-android/mpv-android)
- [yetCalc](https://github.com/Yet-Zio/yetCalc): I like that you can move the cursor :)

## UNIX/Linux

- [Alpine Linux](https://alpinelinux.org): Small distro using busybox and musl. Currently maining, I love the system shell scripts it provides
- [Ardour](https://ardour.org/): Pretty big DAW but I mostly use it to mix voice recordings with some plugins I found from [unfa]($YT/watch?v=0ZWpzAk-2is)
- [Artix Linux](https://artixlinux.org): [Arch](https://archlinux.org) but without systemd
- [FFmpeg](https://ffmpeg.org): Mostly for transcoding and simple video effects (cutting, merging, etc.)
- [Foot](https://codeberg.org/dnkl/foot): Wayland terminal emulator
- [ImageMagick](https://imagemagick.org): For basic image conversion and editing
- [Krita](https://krita.org): Actually really nice to use and featureful drawing/painting program, with some basic image manipulation stuff
- [LibreWolf](https://librewolf-community.gitlab.io): Firefox but better
- [Olive Video Editor](https://olivevideoeditor.org): Actually pretty good (and fast!) video editor
- [aerc](https://aerc-mail.org): "a pretty good email client"
- [aria2](https://aria2.github.io): Terminal downloader, mostly use for torrents.
- [grim](https://wayland.emersion.fr/grim) and [slurp](https://wayland.emersion.fr/slurp): For screenshots
- [imv](https://sr.ht/~exec64/imv): Simple image viewer
- [loksh](https://github.com/dimkr/loksh): Linux port of oksh, I like it's simplicity and the specific way it does vi mode
- [mandoc](https://mandoc.bsd.lv): The mandoc UNIX manpage compiler toolset
- [mpv](https://mpv.io): Best media player, period.
- [newsraft](https://codeberg.org/newsraft/newsraft): Simple Atom/RSS reader
- [nsxiv](https://codeberg.org/nsxiv/nsxiv): Fork of sxiv, mostly use it's gallery view
- [pngquant](https://github.com/kornelski/pngquant): PNG compression by quantization, I smile everytime it halves my PNG sizes :')
- [sway](https://sway.org): Tiling Wayland compositor. Specifically like it's tabbed view and how simple yet "complete" it feels
- [vis](https://sr.ht/~martanne/vis): Modern text editor combining vi and sam. Hits a sweet spot for me of being advanced enough so you save time (structural regex and multi-cursors) but not so advanced that you have to read a big book to use all of it
- [w3m](https://salsa.debian.org/debian/w3m): Terminal browser with images, mostly use when I'm forced to not use a GUI
- [waifu2x-ncnn-vulkan](https://github.com/nihui/waifu2x-ncnn-vulkan): AI Image Upscaler
- [wmenu](https://git.sr.ht/~adnano/wmenu): Very simple menu for Wayland
- [yt-dlp](https://github.com/yt-dlp/yt-dlp): youtube-dl fork with fixes and new features
- [zathura](https://pwmt.org/projects/zathura): Simple document viewer

## Windows

Mostly for when running something proprietary is just more convenient.

- [Clip Studio Paint](https://clipstudio.net/en): Pretty good drawing program (proprietary)
- [Emulsion](https://github.com/ArturKovacs/emulsion): Image Viewer
- [PureRef](https://pureref.com): For viewing reference images (proprietary)
- [Scoop](https://scoop.sh): Package Manager
- [ShareX](https://getsharex.com): Screenshots
- [Waifu2x GUI](https://github.com/AaronFeng753/Waifu2x-Extension-GUI): GUI for waifu2x that has a lot of options and can even upscale video

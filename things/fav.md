# Favorites

This is a collection of some of my favorite things, possibly of all time.

## People

- 4096 ([youtube]($YT/channel/UCTH6s1SMIQicvyd8OLBYMtQ)): Amazing motion graphics, crazy good transitions
- Ado ([youtube]($YT/channel/UCln9P4Qm3-EAY4aiEPmRwEA)): Very well known japanese singer, stupidly good voice
- Alex Hormozi: Very succesful businessman, author, and gym-lover. Has a "no fucks given, just do it" kind of attitude
- Andrew Loomis: *The* 20th century illustrator
- Average Pixel ([youtube]($YT/channel/UCjksxmAxJUlGuXUO1ndt5GA)): Stupidly well edited, high quality, & high effort video game essays. Clearly puts a lot of effort and passion into his work
- BUDO ([twitter]($TWT/t_akemitch)): Japanese anime artist. Lines and shapes are simple and painterly but give a subtle yet powerful feeling of three dimensionality
- Bassetfilms ([twitter]($TWT/Bassetfilms), [youtube]($YT/channel/UC6Qai6akEfmcB67xkTvqXmg))
- Bill Waterson: Known for Calvin & Hobbes. Very fun artstyle with a distinct style of shapes, colors, and textures.
- Brian W. Kernighan: Bell Labs legend and an amazing yet humble teacher, writer, and programmer
- Dondrrr ([twitter]($TWT/dondrrr))
- Dorian Iten ([website](https://dorian-iten.com), [youtube]($YT/channel/UC8Uc-ANlI29P3ToY6EduSqQ)): Great art teacher with "Crystal-clear drawing tips"
- Galneryus: Awesome j-metal band who know how to make some kick-ass music
- George Bridgman: The guy who's sketches confuse most novices. Love his rough sketches and how he simplifies things into more dynamic looking objects
- J.C. Leyendecker: 20th century commercial illustrator. Paintings look real yet shapes are planned so purposefully to make it as appealing as possible
- James Baxter: Traditional western animator. Makes 2D characters look like 3D models, with movements so smooth it makes butter jealous
- Kirby Ferguson ([youtube]($YT/channel/UCu-3YlvaBS4bsya-1eFi62w)): Most known for his *Everything is a Remix* documentary series, great editing & writing
- Kouji Wada: Singer for early Digimon and a childhood legend (RIP)
- Mamoru Hosoda: Directed Wolf Children, Summer Wars, Our War Game, and others. Love the animation style of his films and the fun yet at times heart-wrenching and bittersweet stories told within them
- Mark Manson ([youtube]($YT/channel/UC0TnW9acNxqeojxXDMbohcA)): No-bullshit life advice, most known for his book _The Subtle Art of Not Giving a F*ck_
- Meychan ([youtube]($YT/channel/UCLkxbtUqIudgGSOl8XAdCfA)): Awesome japanese singer, found them through their cover of [buriki no dance]($YT/watch?v=dt8RXLdWiBw)
- Mogoon (モ誰): Japanese illustrator with solid drawings, dynamic compositions, and a stylistic choice of rendering that's very much grounded in real world principles. Also love that subtle painterly texture in his illustrations
- Noah Bradley ([twitter]($TWT/noahbradley/media), [youtube]($YT/channel/UCYu89Q_krburzkoDSpm5xjw)): Painter most known from Dungeons & Dragons, Magic the Gathering, and his Sin of Man project. Fantastic landscape painter in particular, with a large and deep sense of space and very appealing brush strokes
- Norimitsu Suzuki: Basically the japanese Richard Williams; nice shapes, fluid motion, solid drawings, and absolutely mind-boggling perspective shifts
- Richard Williams: Most known for animating Who Framed Rogger Rabbit and The Thief and The Cobbler. Smooth motion, insane perspective shifts, and kind of a hardhead
- Ruan Jia ([twitter]($TWT/RuanJiaJia)): Chinese painter who spends an ungodly amount of time painting characters and scenes with so much dimension and detail it makes a 3D engine look flat in comparison
- Shigenori Soejima: Main artist for the Persona series. Love how graphic and appealing his drawings (and character designs) are but also the way he draws his shapes and paints light and shadow that give his drawings that extra dimension
- Shinichiro Watanabe: Directed Cowboy Bebop, Samurai Champloo, and even some Animatrix episodes
- Studio GAINAX: Personal favorite anime studio. Over-the-top stories and endings with, at times, some of the greatest animation in anime
- Taran Van Hemert ([twitter]($TWT/TaranVH), [youtube]($YT/channel/UCd0ZD4iCXRXf18p3cA7EQfg)): Editor, "macro king", and tree-house and lego builder. I like how he talks and explains technical things
- Tetsuya Nishio: Animator and character designer for Naruto, Ninku, Sky Crawler, and others. Smooth, realistic animation with slick character designs
- Toshihiro Kawamoto: Character designer for Cowboy Bebop, Kekkai Sensen, and others. His character designs are appealing yet have a realistic vibe to them
- Wolpis Kater ([youtube]($YT/channel/UCrFREdqSkLslvG2BpYpZHmg)): Japanese singer, stupidly high notes
- You Yoshinari: Veteran GAINAX animator, directed Little Witch Academia, and character designer of Cyberpunk: Edgerunners. A personal favorite of mine. Especially love his dynamic and appealing shapes, the awesome texture his paintings have, and his killer animation with all their amazing effects
- Yuusuke Murata: Artist of Eyeshield 21 and One-Punch Man. Absolutely inhuman. Dynamic compositions, realistic yet appealing characters, and oh lord the details...
- nujabes: "Godfather" of lo-fi hiphop. Music that make you feel like your floating in an ocean of kickass beats
- the pillows: Japenese alt-rock band most known from FLCL (Fooly Cooly)

## Works

- Children's/Our War Game
- Eureka Seven
- FLCL
- Golden Boy (OVA): Hilarious and insanely well animated
- Gurren Lagann
- Redline
- Rurouni Kenshin
- Summer Wars

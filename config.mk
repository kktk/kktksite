MD=`{find * -type f -name '*.md'}
HTML=${MD:%.md=%.html}
XHTML=${MD:%.md=%.xhtml}

FEED_DIR=feed
AWK=`{find $FEED_DIR -type f -name '*.awk'}
FEED_STEM=${AWK:%.awk=%}
FEED=`{for f in $FEED_STEM; do . ./$f.cfg; printf '%s.%s ' $f "$ext"; done}

CFG=sitecfg.mk

MDPS=md2html
MDPSFLAGS=

BUILD=build

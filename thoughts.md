# Thoughts

*Dates are formatted according to [ISO
8601](https://www.iso.org/iso-8601-date-and-time-format.html)*

2024-01-20:

There are no destinations, we make them up to walk the journey's we
want to walk

2022-12-31:

Lessons from Welcome to The N.H.K. (NHKにようこそ!):

- As long as you're alive, you have power
- It's ok to be lost, everyone is/was
- Life is pain, but that's what makes the happy moments so valuable
- Life won't give you a path, so you'll have to carve one out yourself
- Suicide is never the answer, the only thing it does is permanently stop you from being happy or make others suffer in your place
- There are no shortcuts
- There's always someone who's equal or even worse off than you

2021-09-16:

> "You gotta have something substantial to put out there
> and you'll do fine!... I always put my eggs in 'don't just be
> different, just be good'... Good's different enough."
>
> -Jeff Watts

2021-08-17:

> "My biggest recommendation for learning anatomy is to sculpt,
> rather than draw."
>
> -Dorian Iten

2020-11-3:

> "There's nothing wrong with making bad art, but there is
> something wrong with thinking that your bad art is good."
>
> -Taran Van Hemert

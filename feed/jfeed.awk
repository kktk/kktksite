BEGIN {
	RS="\n\n\n"
	FS="\n"
}

{
	if (NR > 1)
		print ",{"
	else
		print "{"
	gsub("\"", "\\\"")
	print "\t\"title\": \"" $1 "\","
	print "\t\"id\": \"" ENVIRON["SITELINK"] $2 "\","
	if(sub("^!", "", $3))
		print "\t\"url\": \"" $3 "\","
	else
		print "\t\"url\": \"" ENVIRON["SITELINK"] $3 "\","
	split($4, d, "-")
	print "\t\"date_published\": \"" d[1] "-" d[2] "-" d[3] "T" d[4] "Z\","
	getline
	gsub("\"", "\\\"")
	gsub("\n", "\\n")
	gsub("\t", "\\t")
	print "\t\"content_text\": \"" $0 "\""
	print "}"
}

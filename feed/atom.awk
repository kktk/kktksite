BEGIN {
	RS="\n\n\n"
	FS="\n"
}

{
	print "<entry>"
	print "<title>" $1 "</title>"
	print "<id>" ENVIRON["SITELINK"] $2 "</id>"
	if(sub("^!", "", $3))
		print "<link href=\"" $3 "\" />"
	else
		print "<link href=\"" ENVIRON["SITELINK"] $3 "\" />"
	split($4, d, "-")
	print "<updated>" d[1] "-" d[2] "-" d[3] "T" d[4] "Z</updated>"
	print "<content type=\"xhtml\">"
	print "<div xmlns=\"http://www.w3.org/1999/xhtml\">"
	getline
	system("$MDPS $MDPSFLAGS -x <<'EOF'\n" $0 "\nEOF\n")
	print "</div>"
	print "</content>"
	print "</entry>"
}

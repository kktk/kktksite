BEGIN {
	RS="\n\n\n"
	FS="\n"
	split("Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec", MONTH, " ")
}

{
	print "<item>"
	print "<title>" $1 "</title>"
	print "<guid>" ENVIRON["SITELINK"] $2 "</guid>"
	if(sub("^!", "", $3))
		print "<link>" $3 "</link>"
	else
		print "<link>" ENVIRON["SITELINK"] $3 "</link>"
	split($4, d, "-")
	print "<pubDate>" d[3], MONTH[d[2] + 0], d[1], d[4], "+0000</pubDate>"
	print "<description><![CDATA["
	getline
	system("$MDPS $MDPSFLAGS <<'EOF'\n" $0 "\nEOF\n")
	print "]]></description>"
	print "</item>"
}

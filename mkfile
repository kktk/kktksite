<config.mk
<sitecfg.mk

all:V: $HTML $FEED

build:QV: $HTML $FEED
	for f in $prereq; do
		destf="$BUILD/$f"
		destd="$(dirname "$destf")"
		printf '[C] %s\n' "$f"
		mkdir -p "$destd"
		cp -f $f $destf
	done

%.html:Q: $CFG head.html foot.html %.md
	set -a
	case $target in
	*/*) Prefix="$(dirname "$stem" | sed 's/[^/]*/../g')/";;
	esac
	pagetitle="$(sed -n '1 s/\S*\s*// p' "$stem.md" | stev)"
	printf '[M] %s\n' "$target"
	{
		stev < head.html
		stev < "$stem.md" | $MDPS $MDPSFLAGS
		stev < foot.html
	} > "$target"

$FEED:Q: $CFG
	for f in $target; do
		stem="${f%.*}"
		type="${stem##*/}"
		dir="$(dirname "$stem")"
		set -a
		. "./$stem.cfg"
		printf '[M] %s\n' "$f"
		{
			stev < "$FEED_DIR/head.$type"
			stev < "$dir/text" | awk -f "$stem.awk"
			stev < "$FEED_DIR/foot.$type"
		} > "$f"
	done

%.json:Q: %.awk %.cfg
%.xml:Q: %.awk %.cfg
(.*)/(.*)\.(json|xml):R: $FEED_DIR/head.\\2 $FEED_DIR/foot.\\2 \\1/text

clean:V:
	rm -f $HTML $XHTML $FEED

# Global site variables, should be in allcaps
SITELINK="https://kktk.neocities.org/"
SITETITLE="KakaTekel"

PT="https://tube.dsocialize.net"
TWT="https://nitter.net"
YT="https://yewtu.be"

# nav
VIDYT=$YT/channel/UCnMKMbzZwtH_GukDD-pCRGw
VIDPT=$PT/accounts/kakatekel/video-channels

CODE="https://codeberg.org/kktk"
